Bobbit
======

**bobbit** is a simple and modular IRC bot.  The current version uses [Tornado]
as its core networking and event processing backend.

The previous ad-hoc version can be found at
[https://bitbucket.org/pbui/bobbit/](https://bitbucket.org/pbui/bobbit/).

Requirements
------------


Configuration
-------------

Execution
---------

[Tornado]:  http://www.tornadoweb.org/en/stable/
